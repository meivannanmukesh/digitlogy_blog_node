var sql = require('../../config/database.config');
var tableConfig = require('../config/table_config');
var q = require('q');
var moment = require('moment');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const saltRounds = 10;
 

 

 
const path = require('path');
var multer = require('multer');
const next = require('locutus/php/array/next');
const salt = bcrypt.genSaltSync(saltRounds);
// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null,'/uploads')
//     },
//     filename: function (req, file, cb) {
//         var datetimestamp = Date.now();
//         cb(null, file.fieldname + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
//     }
// });
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});
var upload = multer({
    storage: storage
}).single('post_image');
module.exports = {

    validateToken(req,next) {
        //get token from request header
        var deferred = q.defer();
        const authHeader = req.headers["authorization"]
        const token = authHeader!=undefined?authHeader.split(" ")[1]:null
        //the request header contains the token "Bearer <token>", split the string and use the second value in the split array.
        if (token == null) deferred.resolve({ status: 0, message: "Token is not present" });
        jwt.verify(token,'supersecret', (err, user) => {
        if (err) { 
            deferred.resolve({ status: 0, message: "Failed to verify token" });
         }
         else {
         req.user = user
         next() //proceed to the next action in the calling function
         }
        }) //end of jwt.verify()
        return deferred.promise;
        },
    userLogin: (req) => {
        var deferred = q.defer();
        let {email,password}=req.body
         
        
        var checking_query = "select email,id from user where email='"+email+"'";
        sql.query(checking_query, function (err, data) {

            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Error occur to reteive data" });
            } else {
                console.log('data',data)
                if (data.length > 0) {
                    
                    var query="select email,password,first_name,last_name from user where role_id=2 and id='"+data[0].id+"'";
                    sql.query(query,async function(err,response){
                        
                        if (err) {
                            console.log(err);
                            deferred.resolve({ status: 0, message: "Error occur to reteive data" });
                        }
                        if(response.length > 0)
                        {
                            const hash = bcrypt.hashSync('admin123', salt);
                            console.log('password',password,response,response[0].password,hash)
                            let check_password= await bcrypt.compare(password, response[0].password);
                            if(check_password==true)
                            {
                                var token = jwt.sign({ id: response[0].id }, 'supersecret', {
                                    expiresIn: 86400
                                });
                                deferred.resolve({ status: 1, message: "User LoggedIn Successfully",data:{
                                    id:response.length>0?response[0].id:0,
                                    email:response.length>0?response[0].email:'',
                                    username:response.length>0?response[0].first_name +' '+response[0].last_name:'',
                                    token:token
                                } });
                            }
                            else
                            {
                                deferred.resolve({ status: 0, message: "Password is incorrect" });
                            }

                        }
                        else
                        {
                            deferred.resolve({ status: 0, message: "incorrect username" });
                        }

                    })

                } else {
                    deferred.resolve({ status: 0, message: "User is not found" });
                }
            }
        });
        return deferred.promise;
    },

    saveUser: (req) => {
        var deferred = q.defer();

         let {first_name,last_name,email,password,mobile}=req.body
        var role_id=2
        var hashedpassword=bcrypt.hashSync(password, salt);
        var checkingQuery="select * from user where email='"+email+"' and role_id='"+role_id+"' and user_status=0"
        sql.query(checkingQuery,function(err,response)
        {
if(err)
{
    console.log(err)
    deferred.resolve({ status: 0, message: "Failed to check user" });
}
else{
    if(response.length==0)
{
    var saveuserQuery = "INSERT INTO " + tableConfig.USER + " (first_name,last_name,email,password,mobile,role_id) VALUES ('" + first_name + "','" + last_name + "','" + email + "','" + hashedpassword + "', '" + mobile + "','" + role_id + "')";
        sql.query(saveuserQuery, function (err, data) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Failed to save user" });
            } else {
                if (data.affectedRows > 0) {
                   
                    deferred.resolve({ status: 1, message: "User registration completed Successfully" });
                } else {
                    deferred.resolve({ status: 0, message: "Failed to save user" });
                }
            }
        });
}
else
{
    deferred.resolve({ status: 0, message: "Email is already exist" });
}
}
        })
        
        return deferred.promise;
    },

    savePost: (req) => {
        var deferred = q.defer();
        upload(req, next, async function (err) {
            console.log(req)
            let {user_id,post_title,post_slug,post_content,post_summary,post_meta_title,category_id,post_image}=req.body
         var role_id=2
         
        var checkingQuery="select * from user where id='"+user_id+"' and role_id='"+role_id+"' and user_status=0"
        sql.query(checkingQuery,function(err,response)
        {
if(err)
{
    console.log(err)
    deferred.resolve({ status: 0, message: "Failed to check user" });
}
else{
    if(response.length>0)
{
    let imagepath=req.file!=undefined?'public/uploads/'+ req.file.filename:''
    var savepostQuery = "INSERT INTO " + tableConfig.POSTS + " (author_id,post_title,post_slug,post_content,post_summary,post_meta_title,post_image) VALUES ('" + user_id + "','" + post_title + "','" + post_slug + "','" + post_content + "', '" + post_summary + "','" + post_meta_title + "','"+ imagepath + "')";
        sql.query(savepostQuery, function (err, data) {
            console.log(savepostQuery)
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Failed to save post" });
            } else {
                console.log('datta',data)
                if (data.affectedRows > 0) {
                    var savecategoryQuery = "INSERT INTO " + tableConfig.POST_CATEGORY + " (post_id,category_id) VALUES ('" + data.insertId + "','" + category_id + "')";
                    sql.query(savecategoryQuery,function (err,categorydata) {
console.log('cate',categorydata)
                        if(err)
                        {
                            console.log(err)
                            deferred.resolve({ status: 0, message: "Failed to save category" });
                        }
                        if(categorydata.affectedRows > 0)
                        {
                            deferred.resolve({ status: 1, message: "Post published Successfully" });
                        }
                        else
                        {
                            deferred.resolve({ status: 0, message: "Failed to save category" });
                        }
                        
                    })

                   
                } else {
                    deferred.resolve({ status: 0, message: "Failed to save post" });
                }
            }
        });
}
else
{
    deferred.resolve({ status: 0, message: "User is not found" });
}
}
        })
        })
         
        
        return deferred.promise;
    },

    adminLogin: (req) => {
        var deferred = q.defer();

         
        let {email,password}=req.body
        var adminQuery = "select * from " + tableConfig.USER + " where email='" + email + "' and role_id=1 and user_status=0";

        sql.query(adminQuery, async function (err, admindata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (admindata.length > 0) {
                    let check_password= await bcrypt.compare(password, admindata[0].password);
                    if(check_password==true)
                    {
                        deferred.resolve({ status: 1, message: "Admin loggedIn successfully" });
                    }
                    else
                    {
                        deferred.resolve({ status: 0, message: "Incorret password" });
                    }

                     
                    
                } else {
                    deferred.resolve({ status: 0, message: "email is not found" });
                }
            }
        });

        return deferred.promise;
    },

    getallposts: (req) => {
        var deferred = q.defer();
         
        var PostQuery = " select post_title,author_id from " + tableConfig.POSTS + " where post_status=0";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.length > 0) {
                    
                   
                    deferred.resolve({ status: 1, message: "Post list Successfully", posttdata: posttdata });
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },


   getpostsById: (req) => {
        var deferred = q.defer();
         let {post_id}=req.query
        var PostQuery = " select post_title,author_id,post_content from " + tableConfig.POSTS + " where id='"+post_id+"' and post_status=0";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.length > 0) {
                    
                   var UserQuery="select CONCAT(first_name,' ',last_name) as user_name from " + tableConfig.USER + " Where id='"+posttdata[0].author_id+"'"
                   sql.query(UserQuery,function(err,userdata)
                   {
                       
                       if(err)
                       {
                           deferred.resolve({ status: 0, message: "Something wentwrong", error: err })
                       }
                       else
                       {
                            
                           posttdata[0].user_name=userdata.length>0?userdata[0].user_name:'' 
                        deferred.resolve({ status: 1, message: "Post list Successfully", posttdata: posttdata });
                       }
                   })
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },


    deletepostsById: (req) => {
        var deferred = q.defer();
         let {post_id,role,user_id}=req.body
         var condition=''
         if(role==1) //admin
         {
             condition="where id='"+post_id+"'"
         }
         if(role==2)
         {
             condition="where id='"+post_id+"' and author_id='"+user_id+"'"
         }
        var PostQuery = "update  " + tableConfig.POSTS + "  set post_status=1 "+condition+"";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.affectedRows > 0) {
                    
                    deferred.resolve({ status: 1, message: "Deleteion done Successfully" });
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },

    getpostsByFilter: (req) => {
        var deferred = q.defer();
         let {search,value}=req.query
         var condition=''
         if(search==1) //title
         {
             condition="where post_title like '%"+value+"%' and post_status=0"
         }
         if(search==2)
         {
             condition="where author_id='"+value+"' and post_status=0"
         }
        var PostQuery = " select post_title,author_id,post_content from " + tableConfig.POSTS + " "+condition+" ";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.length > 0) {
                    
                   var UserQuery="select CONCAT(first_name,' ',last_name) as user_name from " + tableConfig.USER + " Where id='"+posttdata[0].author_id+"'"
                   sql.query(UserQuery,function(err,userdata)
                   {
                       
                       if(err)
                       {
                           deferred.resolve({ status: 0, message: "Something wentwrong", error: err })
                       }
                       else
                       {
                            
                           posttdata[0].user_name=userdata.length>0?userdata[0].user_name:'' 
                        deferred.resolve({ status: 1, message: "Post list Successfully", posttdata: posttdata });
                       }
                   })
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },

    updatepostsById: (req) => {
        var deferred = q.defer();
         let {post_title,post_content,post_id}=req.body
         var condition=''
         
          
             condition="where id='"+post_id+"' and post_status=0"
         
        var PostQuery = "update  " + tableConfig.POSTS + " set  post_title='"+post_title+"',post_content='"+post_content+"' "+condition+" ";
        sql.query(PostQuery, function (err, posttdata) {
            if (err) {
                console.log(err);
                deferred.resolve({ status: 0, message: "Something Went Wrong" });
            } else {
                if (posttdata.affectedRows > 0) {
                    
                    deferred.resolve({ status: 0, message: "Update Post Successfully"});
                    
                   
                } else {
                    deferred.resolve({ status: 0, message: "No data found", posttdata: [] });
                }
            }
        });
        return deferred.promise;
    },

    

 
    
    


    

         
      
    
}




 