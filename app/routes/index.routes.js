
module.exports = (app) => {

     
    const blogController=require('../controllers/blogController')
     
     
    app.post('/userLogin', blogController.userLogin);
    app.post('/add_post',blogController.vaildatetoken, blogController.savePost);  
    app.post('/user_register', blogController.saveUser);
    app.post('/admin_login', blogController.adminLogin);
    app.put('/update_post', blogController.vaildatetoken,blogController.updatePost)
    app.delete('/delete_post',blogController.vaildatetoken, blogController.deletePosts)
    app.get('/getpost_filter',blogController.vaildatetoken, blogController.getPostsByfilter);
    app.get('/get_post_id', blogController.vaildatetoken, blogController.getpostById);
    app.get('/get_all_posts',blogController.vaildatetoken, blogController.getAllPosts);
};