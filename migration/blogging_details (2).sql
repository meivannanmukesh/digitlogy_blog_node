-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2021 at 11:52 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogging_details`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `meta_title` varchar(224) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `title`, `content`, `status`, `meta_title`) VALUES
(1, 0, 'Social', 'socail titile', 0, 'socailmeta'),
(2, 0, 'travel', 'travel is important', 0, 'travelmeta');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_content` text NOT NULL,
  `post_summary` varchar(255) NOT NULL,
  `post_meta_title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_status` int(11) NOT NULL DEFAULT '0',
  `post_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `post_title`, `post_slug`, `post_content`, `post_summary`, `post_meta_title`, `created_date`, `updated_date`, `post_status`, `post_image`) VALUES
(5, 2, 'travel', 'https://makeawebsitehub.com/examples-of-blogs/', 'travel is important', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Frugal Blogs', '2021-11-18 18:42:49', '2021-11-18 18:42:49', 1, 'public/uploads/post_image-1637288701586.jpg'),
(6, 2, 'Frugal Blogs', 'https://makeawebsitehub.com/examples-of-blogs/', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills etc. Creating a frugal / save money blog can also be a big earner for you, especially if you choose a good angle to take on this niche.\n\nThe following examples all have really good angles. They target a specific demographic and they create content that they will love, share and take action with. Those actions can make you money through affiliate links, Google Ads and coupons.', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Frugal Blogs', '2021-11-19 07:20:45', '2021-11-19 07:20:45', 0, 'public/uploads/post_image-1637288701586.jpg'),
(13, 2, 'Pricing and cost blog ', 'https://makeawebsitehub.com/examples-of-blogs/', 'It used to be that you could gloss over conversations about pricing and cost in marketing materials the way one might sidestep politics and religion at the Thanksgiving dinner table.But today modern digital consumers are no longer content with vague request a quote forms. In fact they expect to be able to easily find content', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Pricing and cost', '2021-11-19 07:55:01', '2021-11-19 07:55:01', 0, 'public/uploads/post_image-1637288701586.jpg'),
(14, 2, 'Pricing and cost blog ', 'https://makeawebsitehub.com/examples-of-blogs/', 'It used to be that you could gloss over conversations about pricing and cost in marketing materials the way one might sidestep politics and religion at the Thanksgiving dinner table.But today modern digital consumers are no longer content with vague request a quote forms. In fact they expect to be able to easily find content', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Pricing and cost', '2021-11-19 08:21:45', '2021-11-19 08:21:45', 0, 'public/uploads/post_image-1637290305291.jpg'),
(20, 2, 'Pricing and cost blog ', 'https://makeawebsitehub.com/examples-of-blogs/', 'It used to be that you could gloss over conversations about pricing and cost in marketing materials the way one might sidestep politics and religion at the Thanksgiving dinner table.But today modern digital consumers are no longer content with vague request a quote forms. In fact they expect to be able to easily find content', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Pricing and cost', '2021-11-19 14:16:36', '2021-11-19 14:16:36', 0, ''),
(21, 2, 'Pricing and cost blog ', 'https://makeawebsitehub.com/examples-of-blogs/', 'It used to be that you could gloss over conversations about pricing and cost in marketing materials the way one might sidestep politics and religion at the Thanksgiving dinner table.But today modern digital consumers are no longer content with vague request a quote forms. In fact they expect to be able to easily find content', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Pricing and cost', '2021-11-19 14:17:23', '2021-11-19 14:17:23', 0, 'public/uploads/post_image-1637311642761.jpg'),
(22, 2, 'Pricing and cost blog ', 'https://makeawebsitehub.com/examples-of-blogs/', 'It used to be that you could gloss over conversations about pricing and cost in marketing materials the way one might sidestep politics and religion at the Thanksgiving dinner table.But today modern digital consumers are no longer content with vague request a quote forms. In fact they expect to be able to easily find content', 'Frugality blogs have really taken off in the past 3 years, mainly down to the state of the global economy. Saving money and being frugal with your wages is extremely popular as people have less and less money these days after paying rent, mortgage, bills ', 'Pricing and cost', '2021-11-19 16:11:28', '2021-11-19 16:11:28', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE `post_category` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `post_id`, `category_id`, `status`) VALUES
(5, 5, 2, 0),
(6, 6, 2, 0),
(13, 13, 2, 0),
(14, 14, 2, 0),
(20, 20, 2, 0),
(21, 21, 2, 0),
(22, 22, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `status`, `created_date`) VALUES
(1, 'Admin', 0, '2021-11-17 17:37:02'),
(2, 'Author', 0, '2021-11-18 04:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_status` int(11) NOT NULL DEFAULT '0' COMMENT '0-active,1--deleted',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `mobile`, `email`, `password`, `role_id`, `user_status`, `created_date`) VALUES
(1, 'meivannan', 'mukesh', '9003616461', 'mei@gmail.com', '$2b$10$q482dXz2dDfoDCAHDpHzX.ZjyDm1nV/JPngiRbZwLZNcefNu2mHAm', 1, 0, '2021-11-17 17:38:04'),
(2, 'mei', 'martin', '9003616461', 'martin@gmail.com', '$2b$10$q482dXz2dDfoDCAHDpHzX.ZjyDm1nV/JPngiRbZwLZNcefNu2mHAm', 2, 0, '2021-11-18 04:39:03'),
(3, 'john', 'nick', '8928200209', 'john@gmail.com', '$2b$10$KOcHcsu9TVzz8jr76tkS2u2hCijwKGeewiW2BhzGgQxK2pak5RSSC', 2, 0, '2021-11-18 07:08:43'),
(4, 'johnson', 'nick', '8928200209', 'johnson@gmail.com', '$2b$10$punvyYDuAWrTPMbesW6GcO6mnapMYeEVZdpaRYmwgAU6W.92Sw052', 2, 0, '2021-11-19 05:15:58'),
(5, 'johnson', 'nick', '8928200209', 'johnsons@gmail.com', '$2b$10$Hsw2zzRH.4qvHuG21i/FLuLWhWr519Qwh/KiJgTd.AkLiwxejlBzG', 2, 0, '2021-11-19 06:44:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `post_category`
--
ALTER TABLE `post_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `post_category`
--
ALTER TABLE `post_category`
  ADD CONSTRAINT `post_category_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
